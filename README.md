# Albuquerque ARTCC 2020 Website
This repo contains the 2020 rewrite of the Albuquerque ARTCC website. This rewrite contains a major technology shift, from PHP (Laravel) to Javascript (Express).

# License

The Albuquerque ARTCC website is hereby released under the Creative Commons CC BY-NC-SA 4.0 license. Please ensure you are familiar with the license before contributing to this project. A couple of key takeaways:

1. If you choose to share or alter this project, you **MUST** give credit to the contributors of this project.
2. You may **NOT** use any of this project for commercial purposes.
3. If you create a derivitive of this project, that project **MUST** be released under the same license.

https://creativecommons.org/licenses/by-nc-sa/4.0/

# Contributors
The following people have contributed to this project. If you have contributed, but your name is not listed here, or if you are on this list and would like to be removed, please email atm@zabartcc.org.

Daan Janssen  
Austin Robison  
Robby Maura  

Thank you to all contributors, past and present.

# Contributing

## !! You will not be able to contribute until the DB seed is made. Please be patient !!

Prerequisites:
- Most recent LTS of Node.js (v12 until 2020-10-27, then v14), with the corresponding version of npm.
- MongoDB running somewhere the site can access it.
- The database seed (coming soon, I promise!)

If you wish to contribute, please do the following:

1. Clone down the repository to your local machine. 
2. Copy `.env.example` to `.env` and fill it out.
2. Run `npm install`
3. Run `npm run dev`

This will compile all of the browser JS, as well as start the development server. 