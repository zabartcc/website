// Core imports
import express from 'express';
import exphbs from 'express-handlebars';
import cookie from 'cookie-parser';
import env from 'dotenv';
import mongoose from 'mongoose';

// Middleware
import globals from './middleware/globals.js';
import loggedInUser from './middleware/loggedInUser.js';
import sidebarData from './middleware/sidebarData.js';

// Controllers
import StaticController from './controllers/StaticController.js';
import UserController from './controllers/UserController.js';
import ControllerController from './controllers/ControllerController.js';
import PilotOnlineController from './controllers/PilotOnlineController.js';
import AtcOnlineController from './controllers/AtcOnlineController.js';
import HelperController from './controllers/HelperController.js';
import ControllerHoursController from './controllers/ControllerHoursController.js';
import AjaxController from './controllers/AjaxController.js';
// import getDataServers from './helpers/getDataServers.js';
import syncRoster from './helpers/syncRoster.js';

// (async () => {
// 	syncRoster();
// })();

// Setup Express
const app = express();
app.use(cookie());
app.use(express.json());

// Setup Handlebars view engine.
app.engine('hbs', exphbs({
	layoutsDir: './views/layout',
	partialsDir: './views/partial',
	defaultLayout: 'main',
	extname: '.hbs'
}));
app.set('view engine', 'hbs');

app.use('/assets', express.static('./assets'));

// Connect to MongoDB
mongoose.set('toJSON', {virtuals: true});
mongoose.connect('mongodb://localhost:27017/zab', { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.once('open', () => console.log('Successfully connected to MongoDB'));

env.config();

app.use(globals);
// gets the logged in user for the header.
app.use(loggedInUser);
// gets the data for the sidebar
app.use(sidebarData);

app.use('/', StaticController);
app.use('/user', UserController);
app.use('/controllers', ControllerController);
app.use('/pilotonline', PilotOnlineController);
app.use('/atconline', AtcOnlineController);
app.use('/helper', HelperController);
app.use('/session', ControllerHoursController);
app.use('/ajax', AjaxController);

app.get('/syncRoster', async ({res}) => {
	await syncRoster();
	res.sendStatus(200);
});

// import ControllerHours from './models/ControllerHours.js'
// import mysql from  './config/database.js';
// import moment from 'moment';

// app.get('/test', async ({res}) => {
//     const [results, metadata] = await mysql.query("SELECT * FROM controller_hours");
//     let numProcessed = 1;
//     for await (const i of results) {
//         console.log(`Processing record ${numProcessed} of ${results.length}`)
//         const timeStart = moment(i.time_start).utc().unix();
//         const timeEnd = moment(i.time_end).utc().unix();

//         const session = new ControllerHours({
//             cid: i.cid,
//             timeStart,
//             timeEnd,
//             pos: i.position
//         })
//         await session.save();
//         numProcessed++;
//     }
//     res.sendStatus(200);
// })

app.listen('3000', () =>{
	console.log('Listening on port 3000');
});