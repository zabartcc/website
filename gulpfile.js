import gulp from 'gulp';

import { rollup } from 'rollup';
import vue from 'rollup-plugin-vue';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import root from 'rollup-plugin-root-import';
// import { babel } from '@rollup/plugin-babel';
// import { terser } from 'rollup-plugin-terser';

import sass from 'gulp-sass';
import sourcemaps from 'gulp-sourcemaps';

import path from 'path';

import nodemon from 'gulp-nodemon';

const compileSASS = () => {
	return gulp.src("./assets/css/*.scss")
		.pipe(sourcemaps.init())
		.pipe(
			sass({outputStyle: 'compressed'})
		)
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest('./assets/dist'));
};

const compileJSDev = async (done) => {
	const bundle = await rollup(getBundleOptions());

	const outOptions = {
		file: "./assets/dist/scripts.js",
		format: "iife",
		sourcemap: true
	};

	await bundle.generate(outOptions);

	await bundle.write(outOptions);

	done();
};

// const compileJSProd = async (cb) => {
	
// 	const bundle = await rollup(getBundleOptions(false));

// 	const outOptions = {
// 		file: "./assets/js/dist.js",
// 		format: "iife",
// 		sourcemap: true
// 	};

// 	await bundle.generate(outOptions);

// 	await bundle.write(outOptions);

// 	cb();
// }; 

const getBundleOptions = (dev = true) => {
	if(dev) {
		return {
			input: "./assets/js/scripts.js",
			plugins: [
				root({
					root: `${path.resolve()}/assets/js`
				}),
				resolve({ jsnext: true, preferBuiltins: true, browser: true }),
				commonjs(),
				vue(),
			]
		};
	} /* else {
		return {
			input: "./assets/js/scripts.js",
			plugins: [
				resolve(),
				commonjs(),
				vue(),
				babel({
					presets: [
						[
							"@babel/env",
							{
								"modules": false,
								"useBuiltIns": "usage",
								"corejs": 3,
								"targets": "ie 11"
							}
						]
					]
				}),
				terser()
			]

		};
	} */
};

const devServer = done => {
	nodemon({
		script: "app.js",
		ignore: ['./assets'],
		done
	});
};

// export const buildJSDev = gulp.series(compileJSDev);
// export const buildProd = gulp.series(compileJSProd);

export const dev = () => {
	gulp.parallel(compileJSDev, compileSASS)();
	gulp.watch('./assets/js', compileJSDev);
	gulp.watch('./assets/css', compileSASS);
	devServer();
};