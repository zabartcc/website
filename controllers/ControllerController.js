import e from 'express';
const router = e.Router();
// import Certification from '../models/Certification.js';
import User from '../models/User.js';

router.get('/', async (req, res) => {
	const controllers = await User.find().sort({
		rating: 'desc',
		lname: 'asc',
		fname: 'asc'
	}).populate({
		path: 'certifications',
		options: {
			sort: {
				'order': 1
			}
		}
	}).lean({ virtuals: true });

	res.render('controller/index', {
		controllers
	});
});

router.get('/staff', ({res}) => {
	res.send("/staff");
});

router.get('/files', ({res}) => {
	res.send("/files");
});

router.get('/:cid', async (req, res) => {
	const controller = await User.findOne({
		cid: req.params.cid
	}).populate({
		path: "roles",
		options: {
			sort: {
				'order': 1
			}
		}
	}).populate({
		path: 'certifications',
		options: {
			sort: {
				'order': 1
			}
		}
	});
	let sessions = null;
	if(controller) {
		sessions = await controller.getControllerHours();
	}
	res.render('controller/profile', {
		controller: (controller) ? controller.toJSON() : null,
		sessions
	});
});

// router.post('/', async (req, res) => {
// 	let users;
// 	if(req.body.sort === "listing") {
// 		users = await User.find().sort({
// 			rating: 'desc',
// 			lname: 'asc',
// 			fname: 'asc'
// 		}).populate({
// 			path: 'certifications',
// 			options: {
// 				sort: {
// 					'order': 1
// 				}
// 			}
// 		});
// 	} else {
// 		users = await User.find();
// 	}

// 	res.json(users);
// });

// router.post('/counts', async (req, res) => {
// 	const counts = {
// 		home: null,
// 		vis: null
// 	};
// 	counts.home = Object.keys(await User.find({
// 		vis: 0,
// 	})).length;
// 	counts.vis = Object.keys(await User.find({
// 		vis: 1,
// 	})).length;
// 	res.json(counts);
// });

// router.get('/:cid', async (req, res) => {
// 	const user = await User.findOne({
// 		cid: req.params.cid
// 	}).populate({
// 		path: "roles",
// 		options: {
// 			sort: {
// 				'order': 1
// 			}
// 		}
// 	}).populate({
// 		path: 'certifications',
// 		options: {
// 			sort: {
// 				'order': 1
// 			}
// 		}
// 	});
// 	res.json(user);
// });

// router.get('/search/:query', async (req, res) => {
// 	let query = req.params.query || 'all';
// 	if(req.params.query === "all") {
// 		query = /.+/;
// 	}
// 	const users = await User.find({
// 		$or: [
// 			{
// 				'fname': {
// 					$regex: query,
// 					$options: 'i'
// 				}
// 			}, {
// 				'lname': {
// 					$regex: query,
// 					$options: 'i'
// 				}
// 			}, {
// 				'email': {
// 					$regex: query,
// 					$options: 'i'
// 				}
// 			}, {
// 				'oi': {
// 					$regex: query,
// 					$options: 'i'
// 				}
// 			}
// 		]
// 	});
// 	res.json(users);
// });

export default router;