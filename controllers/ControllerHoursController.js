import e from 'express';
const router = e.Router();
import ControllerHours from '../models/ControllerHours.js';
import moment from 'moment';

router.get('/:cid', async (req, res) => {
	const sessions = await ControllerHours.find({
		cid: req.params.cid
	});
	const posdef = {
		del: "del",
		gnd: "gnd",
		twr: "twr",
		dep: "app",
		app: "app",
		ctr: "ctr",
	};
	const sessionTable = {
		gtyear: {},
		total: {},
		sessionCount: sessions.length,
		sessionAvg: 0,
	};
	const months = [];
	const today = moment().utc();
	for(let i = 1; i <= 12; i++){
		months.push(today.clone());
		sessionTable[today.format('YYYYMM')] = {};
		today.subtract(1, 'months');
	}

	sessions.forEach(s => {
		const sl = s.timeEnd - s.timeStart;
		let sm = moment.unix(s.timeStart).format('YYYYMM');
		const posArr = s.pos.split('_');
		const posType = posdef[posArr[posArr.length-1].toLowerCase()];

		if(!sessionTable[sm]) {
			sm = 'gtyear';
		}
		sessionTable[sm][posType] = sessionTable[sm][posType] || 0;
		sessionTable[sm].total = sessionTable[sm].total || 0;
		sessionTable.total[posType] = sessionTable.total[posType] || 0;
		sessionTable.total.total = sessionTable.total.total || 0;
		sessionTable[sm][posType] += sl;
		sessionTable[sm].total += sl;
		sessionTable.total[posType] += sl;
		sessionTable.total.total += sl;
	});

	sessionTable.sessionAvg = Math.round(sessionTable.total.total/sessionTable.sessionCount);

	res.json({sessionTable, months});
});

export default router;