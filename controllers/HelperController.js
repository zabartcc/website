import express from 'express';
const router = express.Router();
import zab from '../config/zab.js';

router.get('/ratings', async ({res}) => {
    res.json(zab.ratings)
})
router.get('/ratingslong', async ({res}) => {
    res.json(zab.ratingsLong)
})

export default router;