import e from 'express';
const router = e.Router();
import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import fetch from 'node-fetch';
import env from 'dotenv';
import User from '../models/User.js';
import PilotOnline from '../models/PilotOnline.js';

env.config();

router.get('/', async ({res}) => {
	const getFlights = await PilotOnline.find().lean();
	const flightsJson = JSON.stringify(getFlights);
	res.render('static/home', {
		flightsJson
	});
});

// TODO: Handle ULS login errors more gracefully, with good feedback to the user.

router.get('/login', async (req, res) => {
	
	const jwk = JSON.parse(process.env.VATUSA_JWK);
	const tokenParts = req.query.token.split('.');

	const sig = Buffer.from(
		crypto.createHmac('sha256', new Buffer.from(jwk.k, 'base64'))
			.update(`${tokenParts[0]}.${tokenParts[1]}`)
			.digest()
	).toString('base64').replace(/\+/g, "-").replace(/\//g, '_').replace(/=+$/g, '');


	if(sig === tokenParts[2]) { // compare our generated signature to the signature recieved from VATUSA.
		const token = JSON.parse(Buffer.from(tokenParts[1], 'base64'));
		if(token.iss === 'VATUSA' && token.aud === 'ZAB') {
			const data = await (await fetch(`https://login.vatusa.net/uls/v2/info?token=${tokenParts[1]}`)).json().catch(err => {
				console.log(err); // you should probably handle the error better than this.
				return false;
			});

			if(data) {
				const user = await User.findOne({cid: data.cid});
	
				if(user === null) {
					console.log(`Recieved login from user ${data.lastname}, ${data.firstname} (${data.cid}). User not active member or visitor, discarding.`);
					res.status(500).json({errCode: 'ERR_BAD_LOGIN'});
					return;
				}
	
				const authToken = jwt.sign({cid: user.cid}, process.env.JWT_SECRET, {
					expiresIn: '30d'
				});
				
				res.cookie('token', authToken, {
					expires: new Date(Date.now() + (60*60*24*30*1000))
				});
				res.redirect('/');
			} else {
				console.log('Bad response from VATUSA, discarding.');
				res.status(500).json({errCode: 'ERR_BAD_VATUSA'});
				return;
			}
		} else {
			console.log('Token not from VATUSA/ZAB, discarding.');
			res.status(500).json({errCode: 'ERR_BAD_TOKEN'});
			return;
		}
	} else {
		console.log(`Our signature didn't match VATUSA, discarding.`);
		res.status(500).json({errCode: 'ERR_BAD_SIG'});
		return;
	}
});

router.get('/logout', (req, res) => {
	res.cookie('token', null);
	res.redirect('/');
});

// import mailer from '../helpers/mailer.js';

// router.get('/emailTest', (req, res) => {
// 	mailer.sendMail({
// 		from: "noreply@zabartcc.org",
// 		to: "austin.robison@zabartcc.org",
// 		subject: "Test",
// 		template: "test",
// 	})
// 		.then(() => res.sendStatus(200))
// 		.catch(err => res.send(err));
// });

export default router;