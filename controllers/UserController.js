// const router = require('express').Router();
// const User = require('../models/User');

import express from 'express';
const router = express.Router();
import User from '../models/User.js';
import dotenv from 'dotenv';
import jwt from 'jsonwebtoken';

dotenv.config();

router.post('/', (req, res) => {
	const theToken = req.body.token;

	if(!theToken) {
		res.sendStatus(401);
	} else {
		jwt.verify(theToken, process.env.JWT_SECRET, async (err, decoded) => {
			if(err) {
				console.log(`Unable to verify token: ${err}`);
				res.sendStatus(401);
			} else {
				const user = await User.findOne({
					cid: decoded.cid
				}).populate('roles');
				
				res.status(200).json(user);
			}
		});
	}
});  

export default router;