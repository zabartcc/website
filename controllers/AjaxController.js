import e from 'express';
const router = e.Router();
import AtcOnline from '../models/AtcOnline.js';
import PilotOnline from '../models/PilotOnline.js';

/**
 * This file contains all the methods that handle AJAX calls from JS (including Vue components).
 * All routes should return their data in plain JSON.
 * 
 * Routes: /ajax/...
 * 
 * /atconline
 *
 */

router.get('/atconline', async (req, res) => {
	const atc = await AtcOnline.find();

	res.json(atc);
});

router.get('/pilotonline', async (req, res) => {
	const pilots = await PilotOnline.find();

	res.json(pilots);
});

export default router;