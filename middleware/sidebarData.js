import PilotOnline from '../models/PilotOnline.js';
import AtcOnline from '../models/AtcOnline.js';
import moment from 'moment';

export default async function(req, res, next) {
	req.app.locals.atcOnline = await AtcOnline.find().lean({virtuals: true});
	req.app.locals.pilotsOnline = await PilotOnline.find().lean({virtuals: true});

	req.app.locals.pilotUpdateTime = moment(new Date()).utc().format('MMM DD, HH:mm[z]');
	req.app.locals.atcUpdateTime = moment(new Date()).utc().format('MMM DD, HH:mm[z]');

	next();
}