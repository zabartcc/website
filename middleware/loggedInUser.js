import jwt from 'jsonwebtoken';
import env from 'dotenv';
import User from '../models/User.js';

env.config();

export default function(req, res, next) {
	const token = req.cookies.token || null;
	if(token) {
		jwt.verify(token, process.env.JWT_SECRET, async (err, decoded) => {
			if(err) {
				req.app.locals.user = null;
			} else {
				const user = await User.findOne({
					cid: decoded.cid
				}).populate('roles');
                
				req.app.locals.user = user.toJSON();				
			}
		});
	} else {
		req.app.locals.user = null;
	}
	
	req.app.locals.user = null;

	next();
}