import zab from '../config/zab.js';

export default function(req, res, next) {
	req.app.locals.ratings = zab.ratings;
	req.app.locals.ratingsLong = zab.ratingsLong;

	next();
}