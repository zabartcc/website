import m from 'mongoose';

const controllerHoursSchema = new m.Schema({
    cid: Number,
    timeStart: Number,
    timeEnd: Number,
    pos: String
}, {
    collection: "controllerHours"
})

export default m.model('ControllerHours', controllerHoursSchema);