import fetch from 'node-fetch';
import env from 'dotenv';

import User from '../models/User.js';

env.config();

export default async () => {
	const counts = {
		newControllers: 0,
		deletedControllers: 0
	};

	const res = await fetch(`https://api.vatusa.net/v2/facility/ZAB/roster/?apikey=${process.env.VATUSA_API_KEY}`)
		.catch(err => console.log(err));

	const data = await res.json();
    
	const activeControllers = [];

	let newUsers = [];

	for(const [k, i] of Object.entries(data)) {
		if(k !== 'testing') { //VATUSA needs to format their data better :/
			User.findOne({
				cid: i.cid
			}).then(async user =>{
				if(user === null) { // user doesn't exist
					newUsers.push(User.create({
						cid: i.cid,
						fname: i.fname,
						lname: i.lname,
						email: i.email,
						rating: i.rating,
						broadcast: i.flag_broadcastOptedIn,
						oi: await generateInitials(i.fname, i.lname),
						vis: 0,
					}));
	
					counts.newControllers++;
	
					// await mailer.sendMail({
					// 	from: 'Albuquerque ARTCC <noreply@zabartcc.org>',
					// 	to: 'test@test.com',
					// 	subject: 'New Membership',
					// 	text: 'testing mailer',
					// });
				}
	
				activeControllers.push(i.cid);
			});
		}
	}       

	await Promise.all(newUsers);

	const homeControllers = await User.find({
		vis: 0
	});

	let oldUsers = [];

	for (const i of homeControllers) {
		if(!activeControllers.includes(i.cid)) {
			i.oi = null;
			oldUsers.push(
				i.save().then(() => {
					i.delete();
				})
			);
			counts.deletedControllers++;
		}
	}

	await Promise.all(oldUsers);

	return counts;
};

const generateInitials = async (fname, lname) => {
	let oi;
	fname = fname.toUpperCase();
	lname = lname.toUpperCase();
	const users = await User.find().lean();
	const oiArr = users.map(user => user.oi);

	oi = fname.slice(0,1)+lname.slice(0,1);
	if(!oiArr.includes(oi)) {
		return oi;
	}
	oi = lname.slice(0,1)+fname.slice(0,1);
	if(!oiArr.includes(oi)) {
		return oi;
	}

	let tryCount = 0;

	do {
		if(tryCount < 10) {
			oi = randomInitials(fname+lname);
		} else {
			oi = randomInitials("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
		}
		tryCount++;
	} while(oiArr.includes(oi) && tryCount < 20);

	return oi;
	
};

const randomInitials = (chars) => {
	return chars.charAt(Math.floor(Math.random() * chars.length))+chars.charAt(Math.floor(Math.random() * chars.length));
};

// const generateOi = async user => {
// 	let oi;
// 	let res;

// 	oi = user.fname.substring(0,1)+user.lname.substring(0,1);
// 	res = await checkOi(oi);
// 	if(!res.length) { //Checks FirstLast
// 		return oi.toUpperCase();
// 	}
    
// 	oi = user.lname.substring(0,1)+user.fname.substring(0,1);
// 	oi.toUpperCase();
// 	res = await checkOi(oi);
// 	if(!res.length) { //Checks LastFirst
// 		return oi.toUpperCase();
// 	}

// 	do {
// 		let randOi = randomOi(2, user.fname+user.lname).toUpperCase();
// 		const randRes = await checkOi(randOi);
// 		oi = randOi;
// 	} while (!randRes.length);

// 	return oi.toUpperCase();
// };

// const checkOi = async oi => {
// 	return await User.find({oi});
// };

// const randomOi = (length, name) => {
// 	let result = "";
// 	for ( var i = 0; i < length; i++ ) {
// 		result += name.charAt(Math.floor(Math.random() * name.length));
// 	}
// 	return result;
// };