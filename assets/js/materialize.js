import './materialize/bin/materialize.js';

export default () => {
	M.Dropdown.init(document.querySelectorAll('.dropdown-left'), {
		coverTrigger: false,
		constrainWidth: false
	});
	M.Dropdown.init(document.querySelectorAll('.dropdown-right'), {
		alignment: 'right',
		coverTrigger: false,
		constrainWidth: false
	});
	M.Sidenav.init(document.querySelectorAll('.sidenav'), {
		edge: 'right'
	});
	
	
	
};
