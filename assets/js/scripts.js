// THIS NEEDS TO BE AT THE TOP, DO NOT MOVE IT
import './materialize/bin/materialize.js';

import Vue from 'vue/dist/vue.esm.browser';
import HelloWorld from './vue/HelloWorld.vue';
import WhosOnline from './vue/sidebar/WhosOnline.vue';
import initMaterialize from './materialize.js';

// console.log(M);

// let hero = document.querySelector('#header_hero');
// hero.className = '';
// hero.classList.add('hero' + Math.floor((Math.random() * 5) + 1));

		
// eslint-disable-next-line no-new
new Vue({
	el: '#container',
	components: {
		HelloWorld,
		WhosOnline
	}
});

document.addEventListener('DOMContentLoaded', () => {
	initMaterialize();
});